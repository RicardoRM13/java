FROM openjdk:8

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

#ENTRYPOINT ["/bin/bash"]
CMD ["java", "-jar", "target/supermkt-0.0.1-SNAPSHOT.jar", "--spring.config.location=application.properties" ]