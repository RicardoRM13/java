package com.ricardorm13.supermarket.API.entity;

import javax.persistence.*;
import java.util.Optional;

@Entity @IdClass(UserGrupoID.class)
@Table(name = "\"User_Grupo\"")
public class UserGrupo
{
    @Id
    @Column(name = "id_grupo", nullable = false)
    private Integer id_grupo;

    @Id
    @Column(name = "id_usuario", nullable = false)
    private Integer id_usuario;

    public Integer getId_grupo() {
        return id_grupo;
    }

    public void setId_grupo(Integer id_grupo) {
        this.id_grupo = id_grupo;
    }

    public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

}




