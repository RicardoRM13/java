package com.ricardorm13.supermarket.API.controller;

import java.nio.channels.UnresolvedAddressException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import com.ricardorm13.supermarket.API.entity.User;
import com.ricardorm13.supermarket.API.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository _userRepository;

/*    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> Get() {
        try{
            return _userRepository
        }catch (Exception e){
            logger.error("erro:  " + e);
            return null;
        }

    }
*/
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> GetByNome(@RequestParam(value = "nome", required = false) String nome) {
        try{
            if (nome != null && !nome.isEmpty()) {
                return _userRepository.findByNome(nome);
            }else {
                return _userRepository.findAll();
            }
        }catch (Exception e){
            logger.error("erro:  " + e);
            return null;
        }

    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> GetById(@PathVariable(value = "id") long id)
    {
        Optional<User> pessoa = _userRepository.findById(id);
        if(pessoa.isPresent())
            return new ResponseEntity<User>(pessoa.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/users", method =  RequestMethod.POST)
    public User Post(@Valid @RequestBody User pessoa)
    {
        return _userRepository.save(pessoa);
    }

    @RequestMapping(value = "/users/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<User> Put(@PathVariable(value = "id") long id, @Valid @RequestBody User newPessoa)
    {
        Optional<User> oldPessoa = _userRepository.findById(id);
        if(oldPessoa.isPresent()){
            User pessoa = oldPessoa.get();
            pessoa.setNome(newPessoa.getNome());
            _userRepository.save(pessoa);
            return new ResponseEntity<User>(pessoa, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<User> pessoa = _userRepository.findById(id);
        if(pessoa.isPresent()){
            _userRepository.delete(pessoa.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
