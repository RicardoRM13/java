package com.ricardorm13.supermarket.API.controller;

import com.ricardorm13.supermarket.API.entity.Grupo;
import com.ricardorm13.supermarket.API.entity.UserGrupo;
import com.ricardorm13.supermarket.API.repository.GrupoRepository;
import com.ricardorm13.supermarket.API.repository.UserGrupoRepository;
import jdk.nashorn.internal.runtime.options.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class UserGrupoController {
    Logger logger = LoggerFactory.getLogger(UserGrupoController.class);

    @Autowired
    private UserGrupoRepository _userGrupoRepository;

    @Autowired
    private GrupoRepository _grupoRepository;

    @RequestMapping(value = "/users/{id_usuario}/grupos", method = RequestMethod.GET)
    public List<Grupo> GetById(@PathVariable(value = "id_usuario") Integer id_usuario) {
        return _grupoRepository.listarGruposusuario(id_usuario);

    }

    @RequestMapping(value = "/users/{id_usuario}/grupos", method =  RequestMethod.POST)
    public UserGrupo Post(@Valid @RequestBody UserGrupo grupo, @PathVariable(value = "id_usuario") Integer id_usuario)
    {
        grupo.setId_usuario(id_usuario);
        return _userGrupoRepository.save(grupo);
    }

    @RequestMapping(value = "/users/{id_usuario}/grupos/{id_grupo}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id_usuario") Integer id_usuario, @PathVariable(value = "id_grupo") Integer id_grupo)
    {
        Optional<UserGrupo> userGrupo = _userGrupoRepository.findUserGrupo(id_usuario, id_grupo);
        if(userGrupo.isPresent()){
            _userGrupoRepository.delete(userGrupo.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
