package com.ricardorm13.supermarket.API.controller;

import com.ricardorm13.supermarket.API.entity.Grupo;
import com.ricardorm13.supermarket.API.entity.User;
import com.ricardorm13.supermarket.API.repository.GrupoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class GrupoController {
    Logger logger = LoggerFactory.getLogger(GrupoController.class);

    @Autowired
    private GrupoRepository _grupoRepository;

    @RequestMapping(value = "/grupos", method = RequestMethod.GET)
    public List<Grupo> GetByNome(@RequestParam(value = "nome", required = false) String nome) {
        try{
            if (nome != null && !nome.isEmpty()) {
                return _grupoRepository.findByNome(nome);
            }else {
                return _grupoRepository.findAll();
            }
        }catch (Exception e){
            logger.error("erro:  " + e);
            return null;
        }

    }

    @RequestMapping(value = "/grupos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Grupo> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Grupo> grupo = _grupoRepository.findById(id);
        if(grupo.isPresent())
            return new ResponseEntity<Grupo>(grupo.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/grupos", method =  RequestMethod.POST)
    public Grupo Post(@Valid @RequestBody Grupo grupo)
    {
        return _grupoRepository.save(grupo);
    }

    @RequestMapping(value = "/grupos/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Grupo> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Grupo newGrupo)
    {
        Optional<Grupo> oldGrupo = _grupoRepository.findById(id);
        if(oldGrupo.isPresent()){
            Grupo grupo = oldGrupo.get();
            grupo.setNome(newGrupo.getNome());
            _grupoRepository.save(grupo);
            return new ResponseEntity<Grupo>(grupo, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/grupos/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<Grupo> grupo = _grupoRepository.findById(id);
        if(grupo.isPresent()){
            _grupoRepository.delete(grupo.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
