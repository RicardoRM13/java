package com.ricardorm13.supermarket.API.controller;

import com.ricardorm13.supermarket.API.entity.User;
import com.ricardorm13.supermarket.API.entity.UserLogin;
import com.ricardorm13.supermarket.API.repository.UserLoginRepository;
import com.ricardorm13.supermarket.API.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ricardorm13.supermarket.API.jwt.config.JwtTokenUtil;
import com.ricardorm13.supermarket.API.entity.JwtRequest;
import com.ricardorm13.supermarket.API.entity.JwtResponse;
import com.ricardorm13.supermarket.API.service.JwtUserDetailsService;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private UserLoginRepository _userLoginRepository;

    @Autowired
    private UserRepository _userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());
        UserLogin userLogin = _userLoginRepository.findByUsername(authenticationRequest.getUsername());
        User user = new User();
        user.setNome(userLogin.getNome());
        user.setUsername(userLogin.getUsername());
        user.setId(userLogin.getId());
        final String token = jwtTokenUtil.generateToken(userDetails, user);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public UserLogin obtemUserLogin(String nome){
        UserLogin user = _userLoginRepository.findByUsername(nome);
        return user;
    }
}