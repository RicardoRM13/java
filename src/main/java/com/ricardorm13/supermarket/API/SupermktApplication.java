package com.ricardorm13.supermarket.API;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermktApplication {

    public static void main(String[] args) {

        SpringApplication.run(SupermktApplication.class, args);
    }

}
