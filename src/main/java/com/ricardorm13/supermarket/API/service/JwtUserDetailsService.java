package com.ricardorm13.supermarket.API.service;

import java.util.ArrayList;

import com.ricardorm13.supermarket.API.controller.JwtAuthenticationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.ricardorm13.supermarket.API.entity.UserLogin;
import com.ricardorm13.supermarket.API.jwt.config.WebSecurityConfig;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    JwtAuthenticationController _jwtAuth;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username != null){
            UserLogin userLogin = _jwtAuth.obtemUserLogin(username);

            WebSecurityConfig webSecurityConfig = new WebSecurityConfig();
            if (userLogin.getUsername().equals(username)) {
                return new User(userLogin.getUsername(), webSecurityConfig.passwordEncoder().encode(userLogin.getPassword()),
                        new ArrayList<>());
            } else {
                throw new UsernameNotFoundException("User not found with username: " + username);
            }
        }else{
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

    }
}