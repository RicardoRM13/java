package com.ricardorm13.supermarket.API.repository;

import com.ricardorm13.supermarket.API.entity.Grupo;
import com.ricardorm13.supermarket.API.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Long> {
    List<Grupo> findByNome(String name);

    @Query(value = "select g.id, g.nome from User_Grupo ug inner join Grupo g on g.id = ug.id_grupo where ug.id_usuario = :id_usuario", nativeQuery = true)
    List<Grupo> listarGruposusuario(@Param(value = "id_usuario") Integer id_usuario);
}
