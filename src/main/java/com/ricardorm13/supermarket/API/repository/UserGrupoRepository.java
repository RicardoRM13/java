package com.ricardorm13.supermarket.API.repository;

import java.util.Optional;
import com.ricardorm13.supermarket.API.entity.UserGrupo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UserGrupoRepository extends JpaRepository<UserGrupo, Long> {

    @Query(value = "select ug.id_usuario, ug.id_grupo from User_Grupo ug where ug.id_usuario = :id_usuario  and ug.id_grupo = :id_grupo", nativeQuery = true)
    Optional<UserGrupo> findUserGrupo(@Param(value = "id_usuario") Integer id_usuario, @Param(value = "id_grupo") Integer id_grupo);
}