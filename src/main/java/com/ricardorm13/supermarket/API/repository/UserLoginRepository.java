package com.ricardorm13.supermarket.API.repository;

import com.ricardorm13.supermarket.API.entity.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
public interface UserLoginRepository extends JpaRepository<UserLogin, Long> {

    //@Query(value = "select * from User where username = ?1", nativeQuery = true)
    UserLogin findByUsername(String username);

}
